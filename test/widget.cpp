//
// Created by Анастасия on 20.10.2020.
//
#include <gtest/gtest.h>

extern "C" {
#include "widget.h"
#include "customer.h"
}
namespace TestNamespace::Widget{
    TEST(widget, ok) {
        ASSERT_EQ(widget_ok(1, 1), 1);
    }

    TEST(testy, not_ok) {
        ASSERT_EQ(widget_ok(1, 2), 0);
    }
}