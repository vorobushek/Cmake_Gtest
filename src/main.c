#include <stdio.h>
#include <stdlib.h>

#include "customer.h"
#include "widget.h"

int main() {
    int i;
    int *a = malloc(sizeof(int) * 10);
    if (!a) return -1; /*malloc failed*/
    for (i = 0; i < 10; i++){
        a[i] = i;
    }
    free(a);
    if (widget_ok(1, 1)) {
        return customer_check(4);
    }
    return 0;
}
