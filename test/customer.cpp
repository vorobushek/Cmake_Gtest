//
// Created by Анастасия on 20.10.2020.
//
#include <gtest/gtest.h>

extern "C" {
#include "customer.h"
}
namespace TestNamespace::Customer{
    TEST(customer, ok) {
        ASSERT_EQ(customer_check(5), 1);
    }

    TEST(testy, not_ok) {
        ASSERT_EQ(customer_check(0), 0);
    }
}


